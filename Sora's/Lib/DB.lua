﻿-- Engine
local S, C, L, DB = unpack(select(2, ...))

-- Begin
DB.GlowTex = "InterFace\\AddOns\\Sora's\\Media\\GlowTex"
DB.Statusbar = "InterFace\\AddOns\\Sora's\\Media\\Statusbar"
DB.AuraFont = "Interface\\Addons\\Sora's\\Media\\ROADWAY.TTF"