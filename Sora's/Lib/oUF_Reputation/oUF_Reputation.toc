## Interface: 80000
## Author: p3lim
## Version: 80000.16-Release
## Title: oUF Reputation
## Notes: Reputation Bar support for oUF layouts.
## RequiredDeps: oUF

## X-Curse-Project-ID: 20341
## X-WoWI-ID: 12017

PARAGON.lua
oUF_Reputation.lua
