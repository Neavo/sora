## Interface: 80000
## Title: Extended Vendor UI |cffffffffv1.8.0
## Notes: Extends the merchant interface for easier browsing.
## Author: Germbread
## Version: 1.8.0
## SavedVariables: EXTVENDOR_DATA
## X-Email: germbread@gmail.com

lib\LibStub.lua
lib\AceLocale-3.0.lua

locale\enUS.lua
locale\zhTW.lua
locale\zhCN.lua

ExtVendor.xml
SellJunkPopup.xml
config\Config.xml
config\about.xml
QuickVendorConfig\QuickVendorConfig.xml
